﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Appstyles
{
	public partial class Message : ContentPage
	{
		public Message()
		{
			InitializeComponent();
			Label1.Text = "hi";
			SubscribeTo();
		}

		void SubscribeTo()
		{
			MessagingCenter.Subscribe<Message>(this, "Change", (obj) => { Label1.Text = "Bye"; });
		}

		void Handle_Clicked(object sender, System.EventArgs e)
		{
			Navigation.PushAsync(new Page2_Yes());
		}
	}
}
