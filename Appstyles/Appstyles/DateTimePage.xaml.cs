﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Appstyles
{
	public partial class DateTimePage : ContentPage
	{
		private bool done = false;
		public DateTimePage()
		{
			InitializeComponent();
		}
		protected override void OnDisappearing()
		{
			done = true;
			base.OnDisappearing();
		}
		protected override void OnAppearing()
		{
			done = false;
			Device.StartTimer(TimeSpan.FromSeconds(1), () =>
			{
				Resources["now"] = DateTime.Now.ToString();
				return !done;
			});
			base.OnAppearing();
		}
	}
}
