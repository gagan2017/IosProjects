﻿using System;
namespace RestServiceExamples
{
	public class UserDetails
	{
		public string name { get; set; }
		public int age { get; set; }
		public string city { get; set; }
	}
}
