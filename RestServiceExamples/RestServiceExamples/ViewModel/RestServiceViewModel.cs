﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RestServiceExamples
{
	public class RestServiceViewModel : INotifyPropertyChanged
	{
		public string name;
		public string Name
		{
			get
			{
				return name;
			}
			set
			{
				name = value;
				OnNotifyChanged();
			}
		}


		public int age;
		public int Age
		{
			get
			{
				return age;
			}
			set
			{
				age = value;
				OnNotifyChanged();
			}
		}

		public string city;
		public string City
		{
			get
			{
				return city;
			}
			set
			{
				city = value;
				OnNotifyChanged();
			}
		}



		public event PropertyChangedEventHandler PropertyChanged;

		void OnNotifyChanged([CallerMemberName] string propName="" )
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propName));
			}
		}


		public async Task GetUsers(string url,string searchKeyword)
		{
			HttpClient client = new HttpClient();
			client.BaseAddress = new Uri(url);

			var response = await client.GetAsync(client.BaseAddress);
			response.EnsureSuccessStatusCode();
			var jsonResult = response.Content.ReadAsStringAsync().Result;
			var user = JsonConvert.DeserializeObject<List<UserDetails>>(jsonResult);
			var userdetail = user.Find((obj) => obj.name.Equals(searchKeyword));
			if (userdetail != null)
			{
				setValues(userdetail);
			}
			else
			{
				setValues(new UserDetails()
				{
					name = "User not found"
						,
					age=0,
					city=""
				});
			}
		}

		void setValues(UserDetails user)
		{
			City = user.city;
			Name = user.name;
			Age = user.age;

		}
}
}
