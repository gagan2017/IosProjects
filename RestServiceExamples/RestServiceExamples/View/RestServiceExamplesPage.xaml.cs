﻿
using RestServiceExamples;
using Xamarin.Forms;

namespace RestServiceExamples1
{
	public partial class RestServiceExamplesPage : ContentPage
	{
		RestServiceViewModel vm;
		public RestServiceExamplesPage()
		{
			vm = new RestServiceViewModel();

			InitializeComponent();
			BindingContext = vm;


		}

		async void Handle_Clicked(object sender, System.EventArgs e)
		{
			var url = string.Format("https://api.myjson.com/bins/11xld7");
			await vm.GetUsers(url,editName.Text);

		}
	}
}
