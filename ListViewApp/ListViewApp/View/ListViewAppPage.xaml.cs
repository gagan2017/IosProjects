﻿using Xamarin.Forms;

namespace ListViewApp
{
	public partial class ListViewAppPage : ContentPage
	{
		ListViewAppPageModel vm;
		void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
		{
			var v=e.Item as Person;
			DisplayAlert("selected", v.name, "OK");
		}

		public ListViewAppPage()
		{
			vm = new ListViewAppPageModel();
			BindingContext = vm;

			InitializeComponent();

		}
	}
}
