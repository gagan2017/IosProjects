﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace ListViewApp
{
	public partial class MAinPage : ContentPage
	{
		public MAinPage()
		{
			InitializeComponent();
			Button ListViews = new Button()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Text="List Example"
			};
			ListViews.Clicked += (sender, e) =>
			{
				Navigation.PushAsync(new ListViewAppPage());


			};


			Button Tabs = new Button()
			{
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
				Text = "Tab Example"
			};
			Tabs.Clicked += (sender, e) =>
			{
				Navigation.PushAsync(new TabbedMainPage());
			};
			var s = new StackLayout();
			s.Children.Add(ListViews);
			s.Children.Add(Tabs);
			Content = s;
		}
	}
}
