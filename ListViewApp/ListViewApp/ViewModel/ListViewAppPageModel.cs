﻿using System;
using System.Collections.Generic;

namespace ListViewApp
{
	public class ListViewAppPageModel
	{
		public int id { get; set; }
		public string name { get; set; }
		public List<Person> Person
		{
			get
			{
				return new List<Person>()
				{
					new Person(){name="one",id=1},
					new Person(){name="two",id=1},
					new Person(){name="three",id=1},
					new Person(){name="four",id=1}
				};
			}
		}
	}
}
