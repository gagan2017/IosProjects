﻿using System;
namespace DataBindingExample
{
	public class User
	{
		public string Name { get; set; }
		public string Email { get; set; }
		public int age { get; set; }
	}
}
