﻿using Xamarin.Forms;

namespace DataBindingExample
{
	public partial class DataBindingExamplePage : ContentPage
	{
		User u;
		public DataBindingExamplePage()
		{
			u = new User();
			InitializeComponent();
			u.Name = "abc";
			u.age = 12;
			u.Email = "something @gmail.com";
			BindingContext = u;
		}
	}
}
