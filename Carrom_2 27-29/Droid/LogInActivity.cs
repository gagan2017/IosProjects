﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Carrom_2.Droid
{
	[Activity(Label = "LogInActivity")]
	public class LogInActivity : Activity
	{
		SessionManager session;

		public override void OnBackPressed()
		{
			base.OnBackPressed();

		}
		protected override  void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.Main);

			session = new SessionManager(this);

			// Get our button from the layout resource,
			// and attach an event to it

			Button Clear = FindViewById<Button>(Resource.Id.start);
			Clear.Visibility = ViewStates.Gone;
			Clear.Text = "Clear";
			if (session.getName().Equals("Gagan"))
			{
				Clear.Visibility = ViewStates.Visible;
			}
			Clear.Click +=async delegate
			{


				List<User> jsons = new List<User>();


				await new JSONAsyncTask().SaveTodoItemAsync(jsons, Constants.details_url);

				StartActivity(typeof(DisplayList));


			};
			Button Recycle = FindViewById<Button>(Resource.Id.recycleView);
			Recycle.Text = "Display";
			Recycle.Click += OnItemClick;


		}

		async void OnItemClick(object sender, EventArgs e)
		{

			ProgressDialog progressDialog = ProgressDialog.Show(this, "Please wait...", "", true);

			JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
			progressDialog.Hide();
			List<User> uname=ConverToJson(json.ToString());
			if (session.GetId() == null)
			{
				User u = uname.Find((obj) => obj.PlayerName.Equals(session.getName()));
				if (u != null)
				{
					session.CreateId(u.PlayerName + "Tarams");
				}
			}

			Intent i = new Intent(this, typeof(RecycleViewActivity));
			i.PutExtra("data1", json.ToString());
			StartActivity(i);



		}
		List<User> ConverToJson(string text)
		{
			/*List<User> l = new List<User>();
			List<User> main=JsonConvert.DeserializeObject<List<User>>(text);
			foreach (User u in main)
			{
				if(u.user.EndTime)
			}*/
			return JsonConvert.DeserializeObject<List<User>>(text);



		}
	}
	public class JSONAsyncTask
	{
		//loading from url
		/*public async Task<string> DownloadHomepage()
		{

			var httpClient = new HttpClient();

			Task<string> contentsTask = httpClient.GetStringAsync(Constants.details_url); // async method!

			string contents = await contentsTask;


			return contents;


		}
		*/


		public async Task<JsonValue> GetData(string url)
		{
			// Create an HTTP web request using the URL:
			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
			request.ContentType = "application/json";
			request.Method = "GET";

			// Send the request to the server and wait for the response:
			using (WebResponse response = await request.GetResponseAsync())
			{
				// Get a stream representation of the HTTP web response:
				using (System.IO.Stream stream = response.GetResponseStream())
				{
					// Use this stream to build a JSON document object:
					JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
					Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

					// Return the JSON document:
					return jsonDoc;
				}
			}
		}
		public async Task SaveTodoItemAsync(List<User> item, string RestUrl)
		{
			// RestUrl = http://developer.xamarin.com:8081/api/todoitems{0}
			var json = JsonConvert.SerializeObject(item);


			var content = new StringContent(json, Encoding.UTF8, "application/json");

			var client = new HttpClient();
			client.MaxResponseContentBufferSize = 256000;

			HttpResponseMessage response = null;

			response = await client.PutAsync(RestUrl, content);
			/*if (isNewItem)
			{
				response = await client.PostAsync(uri, content);
			}
*/

 			 if (response.IsSuccessStatusCode)
			{

			}
}


	}


}
