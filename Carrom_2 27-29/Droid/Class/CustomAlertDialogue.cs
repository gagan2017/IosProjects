﻿using System;
using Android.App;
using Android.Content;
using Android.Widget;

namespace Carrom_2.Droid
{
	public class CustomAlertDialogue
	{
		public static AlertDialog.Builder callAlert(Context con, string title, string message, string displayAfterOK)
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(con);
			alert.SetTitle(title);
			alert.SetMessage(message);

			alert.SetNegativeButton("Cancel", (senderAlert, args) =>
						{
				Toast.MakeText(con, displayAfterOK, ToastLength.Short).Show();
						});


			return alert;
		}
	}
}

