﻿using System;
using Android.Content;
using Android.Preferences;

namespace Carrom_2.Droid
{
	public class SessionManager
	{

		ISharedPreferences pref;
		private  string Key_IS_LOGIN = "IsLoggedIn";
		private string Key_name = "name";
		private string Key_phone="phone";
		private string Key_email = "email";
		private string Key_id = "id";
		private string Key_Passsword = "pwd";

	
		// Editor for Shared preferences
		ISharedPreferencesEditor editor;

		// Context
		Context _context;
		public SessionManager(Context context)
		{
			this._context = context;
			pref = PreferenceManager.GetDefaultSharedPreferences(context);
			editor = pref.Edit();
		}

		public bool isLogIn()
		{
			return Constants.IsLoggedIn;
		}
		public void createLogIn(string email, string password)
		{
			Constants.IsLoggedIn = true;
		
			editor.PutBoolean(Key_IS_LOGIN, true);
			editor.PutString(Key_name, email);
			editor.PutString(Key_Passsword, password);
			editor.Commit();
		}

		public void CreateId(string id)
		{
			editor.PutString(Key_id, id);
			editor.Commit();
		}

		public String GetId()
		{
			var id = pref.GetString(Key_id, null);
			return id;
		}

		public String getName()
		{
			var id=pref.GetString(Key_name, null);
			return id;
		}
		public bool getLogInStatus()
		{
			var id = pref.GetBoolean(Key_IS_LOGIN, false);
			return id;
		}


	}
}
