﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Carrom_2.Droid
{
	[Activity(Label = "Carrom_2", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		EditText email, password;
		Button login;
		SessionManager session;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			session = new SessionManager(this);
			base.OnCreate(savedInstanceState);
			if (session.getLogInStatus() == false)
			{
				SetContentView(Resource.Layout.log_in_layout);
				email = FindViewById<EditText>(Resource.Id.email);
				password = FindViewById<EditText>(Resource.Id.password);

				login = FindViewById<Button>(Resource.Id.logIn);
				login.Click +=  (sender, e) =>
				{
					if (email.Text.Trim().Length == 0 && password.Text.Trim().Length == 0)
					{
						var alert=CustomAlertDialogue.callAlert(this, "Error", "Enter email or password", "Log In again...");


						Dialog dialog = alert.Create();
						dialog.Show();


					}
					else
					{
						
						session.createLogIn(email.Text, password.Text);
						Constants.IsLoggedIn = true;
						var loginactivity = new Intent(this, typeof(LogInActivity));
						StartActivity(loginactivity);
					}

				};
			}
			else
			{
				var loginactivity = new Intent(this, typeof(LogInActivity));

				StartActivity(loginactivity);


			}


		}

	}
}

