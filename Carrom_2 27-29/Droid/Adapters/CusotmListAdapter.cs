﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Carrom_2.Droid
{
	public class CusotmListAdapter : BaseAdapter<User>
	{
		Activity context;
		List<User> list;

		public CusotmListAdapter(Activity _context, List<User> _list)
			: base()
		{
			this.context = _context;
			this.list = _list;
		}

		public override int Count
		{
			get { return list.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override User this[int index]
		{
			get { return list[index]; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			// re-use an existing view, if one is available
			// otherwise create a new one
			if (view == null)
				view = context.LayoutInflater.Inflate(Resource.Layout.RowLayout, parent, false);

			User item = this[position];
			view.FindViewById<TextView>(Resource.Id.Title).Text ="Name :"+ item.PlayerName;
			view.FindViewById<TextView>(Resource.Id.Description).Text = "No Of Games Played Today :"+(item.GamesPlayedToday+1).ToString();


			return view;
		}
	}
}
