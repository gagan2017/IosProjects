﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Newtonsoft.Json;
using Org.Apache.Http.Client.Methods;

namespace Carrom_2.Droid
{
	
	[Activity(Label = "RecycleViewActivity")]
	public class RecycleViewActivity : Activity
	{
		SessionManager session;
		Button add;
		string text;
		ListView listView;
		SwipeRefreshLayout refresher;
		List<User> listData;
		ProgressDialog progressDialog;
		protected override  void OnCreate(Bundle savedInstanceState)
		{
			listData = new List<User>();
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.DisplayList);
			listView = FindViewById<ListView>(Resource.Id.ListView);


			 text = Intent.GetStringExtra("data1") ?? "Data not available";
			listData = ConverToJson(text);
			session = new SessionManager(this);
			var s = session.getName();
			refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
			refresher.SetColorScheme(Resource.Color.material_blue_grey_800);
			refresher.Refresh += OnRefresh;

			 add = FindViewById<Button>(Resource.Id.Add);
			initHotelList();



			AddButon();
		
			add.Click += (sender, e) =>
			{




				var alert = CustomAlertDialogue.callAlert(this, "Confirm", "Play now", "start "+s);
				alert.SetPositiveButton("Ok", async (senderAlert, args) =>
						{

					List<User> jsons = JsonConvert.DeserializeObject<List<User>>(text);
							UserDetails ud = new UserDetails();
							ud.timeStart = DateTime.Now;
							User u= new User();
							
							if (session.GetId() == null)
							{
								
								u.id = session.getName()+"Tarams";
								u.PlayerName = session.getName();
								
								
									ud.id = u.user.Count;

									u.user.Add(ud);
								
								jsons.Add(u);	

								session.CreateId(u.id);
							}
							else
							{
								var userob=	jsons.Find(userobj => userobj.id.Equals(session.GetId()));
								jsons.Remove(userob);
								userob.GamesPlayedToday = userob.user.Count;
								ud.id = userob.user.Count;
								userob.user.Add(ud);
								jsons.Add(userob);
								
								
							}
							
							progressDialog = ProgressDialog.Show(this, "Please wait...", "Adding to List...", true);
							await new JSONAsyncTask().SaveTodoItemAsync(jsons, Constants.details_url);
							
							OnRefresh(sender,e);
						

				});
				Dialog dialog = alert.Create();
				dialog.Show();

			};
		


		}

		async void OnRefresh(object sender, EventArgs e)
		{
			
			JsonValue v = await new JSONAsyncTask().GetData(Constants.details_url);

			text = v.ToString();
			listData = ConverToJson(text);
			AddButon();
			initHotelList();

			progressDialog.Hide();
			refresher.Refreshing = false;
		}

		List<User> ConverToJson(string text)
		{
			List<User> l = new List<User>();
			List<User> main=JsonConvert.DeserializeObject<List<User>>(text);
			/*foreach (User u in main)
			{
				if
			}*/
			return JsonConvert.DeserializeObject<List<User>>(text);



		}
		public void AddButon()
		{
			List<User> user = new List<User>();

			foreach (var item in listData)
			{

				if(!CheckTime(item)){
					user.Add(item);
				}
			}
			if (user.Count != 0)
			{
				foreach (var item in user)
				{
					listData.Remove(item);
				}
			}

			User u1 = listData.Find((obj) => obj.id.Equals(session.GetId()));
			if (u1 != null)
			{
				if (CheckTime(u1))
				{
					add.Clickable = false;
					add.Text = "cant add wait for 30 mins!";
				}
				else
				{
					add.Clickable = true;
					add.Text = "PlayNow";
					//add.Visibility = ViewStates.Visible;
				}
			}
		}
		bool CheckTime(User u1)
		{
			DateTime t = u1.user[u1.user.Count - 1].timeStart;

			if (t.Hour == DateTime.Now.Hour && t.Minute > DateTime.Now.Minute - 3)
			{

			}
			else
			{

				return false;
			}
			return true;
		}
		public void initHotelList()
		{
			
			listView.Adapter = new CusotmListAdapter(this, listData);
		}




	}






}
