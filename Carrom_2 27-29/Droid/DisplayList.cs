﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Carrom_2.Droid
{
	[Activity(Label = "DisplayList")]
	public class DisplayList : ListActivity
	{
		string[] items;
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			items = new string[] { "a", "b", "c d", "e", "f", "g" };
			ListAdapter = new DisplayAdapter(this, items);

		}
		protected override void OnListItemClick(ListView l, View v, int position, long id)
		{
			var t = items[position];
			Toast.MakeText(this, t, ToastLength.Short).Show();
		}
	}
}
