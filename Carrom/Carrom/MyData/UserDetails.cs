﻿using System;
namespace Carrom
{
	public class UserDetails
	{
		public int id { get; set; }
		public string name { get; set; }
		public string phone { get; set; }
		public int noOfGamesPlayed { get; set; }
	}
}
