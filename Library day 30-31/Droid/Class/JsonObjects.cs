﻿using System;
using System.Collections.Generic;

namespace Carrom_2.Droid
{
	public class JsonObjects
	{
		public JsonObjects()
		{
			user = new List<LibraryUser>();
			book = new List<Book>();
			user_book = new List<UserBook>();

		}
		public List<LibraryUser> user;
		public List<Book> book;
		public List<UserBook> user_book;
	}
}
