﻿using System;
using System.Collections.Generic;

namespace Carrom_2.Droid
{

	public class Time
	{
		public int Hr { get; set; }
		public int min { get; set; }

	}
   public class UserDetails
	{
		public DateTime timeStart;

		public int id { get; set; }


	}

	public class User
	{
		public User()
		{
			user = new List<UserDetails>();
		}
		
		public string id { get; set; }
		public string PlayerName { get; set; }
		public int GamesPlayedToday { get; set; }
		public List<UserDetails> user;

	}
}
