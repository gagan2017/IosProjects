﻿using System;
namespace Carrom_2.Droid
{
	public class Book :MainIdName
	{
		public string Title{ get; set; }
		public string BookDescription{ get; set; }
		public string ImageUrl{ get; set; }
	}
}
