﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Carrom_2.Droid
{
	[Activity(Label = "LibraryLogInActivity")]
	public class LibraryLogInActivity : Activity
	{
		SessionManager session;
		Button add,display;
		string text;
		ListView listView;
		SwipeRefreshLayout refresher;

		ProgressDialog progressDialog;
		List<Book> bookList;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Layout_Library_LogIn);
			// Create your application here
			var text = Intent.GetStringExtra("text");


			if (text == null)
			{
				(CustomAlertDialogue.callAlert(this, "internet eror", "Title", "Thank you")).Show();
			}
			else
			{
				bookList = JsonConvert.DeserializeObject<JsonObjects>(text).book;
				listView = FindViewById<ListView>(Resource.Id.BookList);
				refresher = FindViewById<SwipeRefreshLayout>(Resource.Id.refresher);
				refresher.SetColorScheme(Resource.Color.material_blue_grey_800);
				refresher.Refresh += OnRefresh;

				add = FindViewById<Button>(Resource.Id.AddBooks);
				add.Click += (sender, e) =>
				{
					//StartActivity(typeof(AddBookActivity));

					Dialog d = new Dialog(this);
					d.SetContentView(Resource.Layout.Layout_AddBook);
					d.Show();
					EditText BookName, BookTitle, BookDescription, BookUrl;
					Button addbook;
					BookName = d.FindViewById<EditText>(Resource.Id.BookName);
					BookTitle = d.FindViewById<EditText>(Resource.Id.BookTitle);
					BookDescription = d.FindViewById<EditText>(Resource.Id.BookDescription);
					BookUrl = d.FindViewById<EditText>(Resource.Id.BookUrl);
					addbook = d.FindViewById<Button>(Resource.Id.AddBook);

					addbook.Click += async (sender1, e1) => 
					{
						ProgressDialog progressDialog = new ProgressDialog(this);
						progressDialog.SetTitle("Please Wait");
						progressDialog.Show();
						JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
						JsonObjects j = JsonConvert.DeserializeObject<JsonObjects>(json.ToString());
						j.book.Add(new Book()
						{
							Name = BookName.Text,
							Title = BookTitle.Text,
							BookDescription = BookDescription.Text,
							Id = j.book.Count+1,
							ImageUrl = BookUrl.Text
						});

						await new JSONAsyncTask().SaveTodoItemAsync1(j, Constants.details_url);
						progressDialog.Hide();

						d.Dismiss();
						OnRefresh(sender1,e1);
					};


				};



				display = FindViewById<Button>(Resource.Id.display);
				display.Click += async(sender, e) =>
				{
					Dialog d = new Dialog(this);
					d.SetContentView(Resource.Layout.DisplayList);
					ListView l = d.FindViewById<ListView>(Resource.Id.ListView);
					Button play = d.FindViewById<Button>(Resource.Id.Add);
					play.Visibility = ViewStates.Gone;
				
					ProgressDialog progressDialog = new ProgressDialog(this);
					progressDialog.SetTitle("Please Wait");
					progressDialog.Show();
					JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
					text = json.ToString();
					List<UserBook> ub = JsonConvert.DeserializeObject<JsonObjects>(text).user_book;

					l.Adapter = new LibraryCustomListUserAdapter(this, ub);
					progressDialog.Hide();
					d.Show();
				};

				initHotelList();
			}






		}

		async void OnRefresh(object sender, EventArgs e)
		{
			ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.SetTitle("Please Wait");
			progressDialog.Show();
			JsonValue v = await new JSONAsyncTask().GetData(Constants.details_url);
			text = v.ToString();
			bookList = JsonConvert.DeserializeObject<JsonObjects>(text).book;
			initHotelList();

			progressDialog.Hide();
			refresher.Refreshing = false;
		}
		public void initHotelList()
		{

			listView.Adapter = new LibraryCustomListBookAdapter(this, bookList);

			listView.ItemClick += async(object sender, AdapterView.ItemClickEventArgs e) =>
				{
					EditText euserName;
					LibraryUser luobj;
				Book b = bookList[e.Position];
				Dialog d = new Dialog(this);
				d.SetContentView(Resource.Layout.Layout_Display_Detail);
				TextView name, description, image, authorname;
				Button assign;
				d.Show();
				name = d.FindViewById<TextView>(Resource.Id.detailName);
				description = d.FindViewById<TextView>(Resource.Id.detailsDescription);
				image = d.FindViewById<TextView>(Resource.Id.detailImage);
				authorname = d.FindViewById<TextView>(Resource.Id.authorName);
					euserName = d.FindViewById<EditText>(Resource.Id.editUserName);

				assign = d.FindViewById < Button>(Resource.Id.detailAssign);
				name.Text = "Name :"+b.Title;
				description.Text = "Bescription :" + b.BookDescription;
				image.Text = "Image :" + b.ImageUrl;
				authorname.Text = "Author Name :" + b.Name;

				ListView l;
					l = d.FindViewById<ListView>(Resource.Id.DetailuserList);
					ProgressDialog progressDialog = new ProgressDialog(this);
					progressDialog.SetTitle("Please Wait");
					progressDialog.Show();
					JsonValue v = await new JSONAsyncTask().GetData(Constants.details_url);
					text = v.ToString();

					List<LibraryUser> ulist = JsonConvert.DeserializeObject<JsonObjects>(text).user;


					l.Adapter = new LibraryCustomUserListAdapter(this, ulist);
							
					
					progressDialog.Hide();
				l.ItemClick+= (sender2, e2) => {

					luobj = ulist[e2.Position];
					euserName.Text = luobj.Name;

				};	

				assign.Click +=async(sender1, e1) =>
					{
						luobj=ulist.Find((obj) => obj.Name.Equals(euserName.Text));
						if (luobj == null)
						{
							
						}
						else
						{
							UserBook ub1=new UserBook();
							ub1.userid = luobj;
							ub1.bookid =b;
							JsonObjects j=	JsonConvert.DeserializeObject<JsonObjects>(text);
							j.user_book.Add( ub1);
							ProgressDialog progressDialog1 = new ProgressDialog(this);
							progressDialog1.SetTitle("Please Wait adding book to user");
							progressDialog1.Show();
							await new JSONAsyncTask().SaveTodoItemAsync1(j, Constants.details_url);
							progressDialog1.Hide();
							d.Dismiss();
							OnRefresh(sender1, e1);
						}
					};


				};

		}
	}
}
