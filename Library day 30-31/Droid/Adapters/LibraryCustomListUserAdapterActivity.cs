﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Carrom_2.Droid
{
	public class LibraryCustomListUserAdapter : BaseAdapter<UserBook>
	{
		Activity context;
		List<UserBook> list;


		public LibraryCustomListUserAdapter(Activity _context, List<UserBook> _list)
			: base()
		{
			this.context = _context;
			this.list = _list;

		}




		public override int Count
		{
			get { return list.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override UserBook this[int index]
		{
			get { return list[index]; }
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			// re-use an existing view, if one is available
			// otherwise create a new one
			if (view == null)
				view = context.LayoutInflater.Inflate(Resource.Layout.Layout_Library_Details, parent, false);

			UserBook item = this[position];
			view.FindViewById<TextView>(Resource.Id.BookTitle).Text = "Name :" + item.userid.Name;
			view.FindViewById<TextView>(Resource.Id.BookDescription).Text = "Book Taken:" + item.bookid.Name;
			return view;

		}
	}
}
