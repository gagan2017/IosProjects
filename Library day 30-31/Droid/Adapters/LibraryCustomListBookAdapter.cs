﻿using System;

using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
namespace Carrom_2.Droid
{
	public class LibraryCustomListBookAdapter:BaseAdapter<Book>
	{
		Activity context;
		List<Book> list;
	

		public LibraryCustomListBookAdapter(Activity _context, List<Book> _list)
			: base()
		{
			this.context = _context;
			this.list = _list;

		}




		public override int Count
		{
			get { return list.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override Book this[int index]
		{
			get { return list[index]; }
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			// re-use an existing view, if one is available
			// otherwise create a new one
			if (view == null)
				view = context.LayoutInflater.Inflate(Resource.Layout.Layout_Library_Details, parent, false);
			
				Book item = this[position];
				view.FindViewById<TextView>(Resource.Id.BookTitle).Text = "Name :" + item.Title;
				view.FindViewById<TextView>(Resource.Id.BookDescription).Text = "Description\n" + item.BookDescription;
				return view;
		
		}
	}
}
