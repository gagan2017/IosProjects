﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;

namespace Carrom_2.Droid
{
	public class LibraryCustomUserListAdapter: BaseAdapter<LibraryUser>
	{
		Activity context;
		List<LibraryUser> list;


		public LibraryCustomUserListAdapter(Activity _context, List<LibraryUser> _list)
			: base()
		{
			this.context = _context;
			this.list = _list;

		}




		public override int Count
		{
			get { return list.Count; }
		}

		public override long GetItemId(int position)
		{
			return position;
		}

		public override LibraryUser this[int index]
		{
			get { return list[index]; }
		}


		public override View GetView(int position, View convertView, ViewGroup parent)
		{
			View view = convertView;

			// re-use an existing view, if one is available
			// otherwise create a new one
			if (view == null)
				view = context.LayoutInflater.Inflate(Resource.Layout.Layout_Library_Details, parent, false);

			LibraryUser item = this[position];
			view.FindViewById<TextView>(Resource.Id.BookTitle).Text = "Name :" + item.Name+"\tEmail:" + item.email; ;
			view.FindViewById<TextView>(Resource.Id.BookDescription).Text = "";
			return view;

		}
	}
}
