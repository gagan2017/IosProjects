﻿
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Carrom_2.Droid
{
	[Activity(Label = "AddBookActivity")]
	public class AddBookActivity : Activity
	{
		EditText BookName, BookTitle, BookDescription, BookUrl;
		Button addbook;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Layout_AddBook);
			BookName = FindViewById<EditText>(Resource.Id.BookName);
			BookTitle = FindViewById<EditText>(Resource.Id.BookTitle);
			BookDescription = FindViewById<EditText>(Resource.Id.BookDescription);
			BookUrl = FindViewById<EditText>(Resource.Id.BookUrl);
			addbook = FindViewById<Button>(Resource.Id.AddBook);

			addbook.Click += async delegate
			{
				JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
				JsonObjects j = JsonConvert.DeserializeObject<JsonObjects>(json.ToString());
				j.book.Add(new Book()
				{
					Name = BookName.Text,
					Title = BookTitle.Text,
					BookDescription = BookDescription.Text,
					Id = 2,
					ImageUrl = BookUrl.Text
				});
				ProgressDialog progressDialog = new ProgressDialog(this);
				progressDialog.SetTitle("Please Wait");

				await new JSONAsyncTask().SaveTodoItemAsync1(j, Constants.details_url);
				progressDialog.Hide();
			};

		
		}
	}
}
