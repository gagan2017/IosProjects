﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using System.Json;
using Newtonsoft.Json;

namespace Carrom_2.Droid
{
	[Activity(Label = "Carrom_2", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		EditText email, password;
		Button login,create,signup,Display;
		string text = "";
		SessionManager session;
		Intent loginactivity;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			session = new SessionManager(this);
			SetContentView(Resource.Layout.log_in_layout);
			// Create your application here
			email = FindViewById<EditText>(Resource.Id.email);
			password = FindViewById<EditText>(Resource.Id.password);


			signup = FindViewById<Button>(Resource.Id.signup);
			signup.Click += (sender, e) =>
			{
				Button sign;
				EditText name, phone, email, password;

				Dialog d = new Dialog(this);

				d.SetContentView(Resource.Layout.Layout_SignUp);

				name = d.FindViewById<EditText>(Resource.Id.UserName);
				phone = d.FindViewById<EditText>(Resource.Id.UserPhone);
				email = d.FindViewById<EditText>(Resource.Id.UserEmail);
				password = d.FindViewById<EditText>(Resource.Id.UserPassword);
				sign = d.FindViewById<Button>(Resource.Id.sign_up);
				sign.Click += async (senders, ee) =>
				{
					ProgressDialog progressDialog = new ProgressDialog(this);
					progressDialog.SetTitle("Creating Please Wait");
					progressDialog.Show();
					JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
					JsonObjects j = JsonConvert.DeserializeObject<JsonObjects>(json.ToString());
					j.user.Add(new LibraryUser()
					{
						Id=j.user.Count+1,Name=name.Text,
						Phone=int.Parse(phone.Text),
						email=email.Text

					});
					await new JSONAsyncTask().SaveTodoItemAsync1(j, Constants.details_url);
					progressDialog.Hide();
					d.Hide();

				};
				d.Show();

			};


			create = FindViewById<Button>(Resource.Id.Create);
			create.Click += async delegate {
				
				JsonObjects j = new JsonObjects();
				Book b = new Book()
				{
					Id = 1,
					Name = "AuthorName",
					BookDescription = "BookDescription",
					ImageUrl = "ImageUrl",
					Title = "BookTitle"
				};

				j.book.Add(b);
				LibraryUser u = new LibraryUser()
				{
					Id = 1,
					Name = "Admin",
					Phone = 1234567890,
					email = "Admin"
				};

				j.user.Add(u);


				j.user_book.Add(new UserBook()
				{
					id = 1,
					userid =u ,
					bookid = b
				});

				string s=JsonConvert.SerializeObject(j);
				await new JSONAsyncTask().SaveTodoItemAsync1(j, Constants.details_url);


			};
			if (session.getLogInStatus() == true)
			{

				SetContentView(Resource.Layout.Layout_Display);

				Display = FindViewById<Button>(Resource.Id.displaynext);
				Display.Click += async delegate
				{
					ProgressDialog progressDialog = new ProgressDialog(this);
					progressDialog.SetTitle("Please Wait");
					progressDialog.Show();
					JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);

					progressDialog.Hide();
					text = json.ToString();
					Intent i = new Intent(this, typeof(LibraryLogInActivity));
					i.PutExtra("text", text);

					StartActivity(i);
				};

			}
			else
			{





				login = FindViewById<Button>(Resource.Id.logIn);

				login.Click += async delegate
				{

					ProgressDialog progressDialog = new ProgressDialog(this);
					progressDialog.SetTitle("Please Wait");
					progressDialog.Show();
					if (ValidateEmail())
					{
						JsonValue json = await new JSONAsyncTask().GetData(Constants.details_url);
						text = json.ToString();
						JsonObjects j = JsonConvert.DeserializeObject<JsonObjects>(text);
						LibraryUser u = j.user.Find((obj) => obj.email.Equals(email.Text));
						if (u.email.Equals(email.Text))
						{

							string userjson = JsonConvert.SerializeObject(u);
							session.UserString(userjson);


							Intent i = new Intent(this, typeof(LibraryLogInActivity));
							Constants.IsLoggedIn = true;

							if (u.email.Equals("Admin"))
							{
								session.createLogIn(email.Text, password.Text, true);
							}
							else
							{
								session.createLogIn(email.Text, password.Text, false);
							}
							i.PutExtra("text", text);
							progressDialog.Hide();
							StartActivity(i);

						}
					}
					progressDialog.Hide();
				};
			}

		}
		bool ValidateEmail()
		{
			if (email.Text.Trim().Length == 0 && password.Text.Trim().Length == 0)
			{
				var alert = CustomAlertDialogue.callAlert(this, "Error", "Enter email or password", "Log In again...");



				Dialog dialog = alert.Create();
				dialog.Show();

				return false;
			}
			return true;
		}

	}
}

